/*
* #subnets creation
* Public subnet creaton
*/
resource "aws_subnet" "public_subnet" {
  vpc_id = aws_vpc.myvpc.id
  cidr_block = var.public_subnet_cidr
  availability_zone = var.public_subnet_avilability_zone
  tags = {
    Name = "public_subnet"
  }
}


/* #subnets creation
* private subnet creaton
*/

resource "aws_subnet" "private_subnet" {
  
vpc_id = aws_vpc.myvpc.id
cidr_block = var.private_subnet_cidr
availability_zone = var.private_subnet_avilability_zone
tags = {
  Name = "private_subnet"
  }
}
