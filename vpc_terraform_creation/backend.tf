terraform {
  backend "s3" {
    bucket = "love-terraform-state"
    key = "path/to/my/terraform_state_file"
    region = "us-east-1"
    //dynamodb_table = "dynamodb-01"
  }

}