variable "region" {
  
}

variable "vpc_cidr" {
  
}

variable "public_subnet_cidr" {
  
}

variable "public_subnet_avilability_zone" {
  
}

variable "private_subnet_cidr" {
  
}

variable "private_subnet_avilability_zone" {
  
}

variable "dst_cidr_block" {
    
}
/* Instance variables*/

variable "instance" {
  type = map(object({
  ami = string
  avz = string
  kname = string
  tag  = string
  itype = string
  }))
  default = {
    "instance-1" = {
      ami = "ami-033b95fb8079dc481"
      avz = "us-east-1a"
      kname = "my-test-vpc-key-01"
      tag = "jenkins"
      itype = "t2.micro"
    }
  }
}

variable "sg" {
  type = map(object({
   port = number
   protocol = string
  name = string
   cidr_block = list(string)

  }))
default =  {
  "sg-1" = {
   port = 22
   protocol = "tcp"
   name = "my-sec-1"
   cidr_block = ["0.0.0.0/0"]
   }

   /*"sg-2" ={
    port = 8080
   protocol = "tcp"
   //name = "my-sec-1"
   cidr_block = ["0.0.0.0/0"]

   } */
  } 
 }