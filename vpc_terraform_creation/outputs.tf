output "vpcid" {
  value = aws_vpc.myvpc.id
}

#output "My_security_group" {
# value = values(aws_security_group.my-sec-myvpc)[*].id
# value = aws_security_group.my-sec-myvpc["sg-1"].id
# value =  aws_security_group.my-sec-myvpc["sg-1"].ingress
#}


output "my-sgs" {

 // value = element([ for v in aws_security_group.my-sec-myvpc : v.id ], 0 )
 value = "${aws_security_group.my-sec-myvpc}"
  
}

output "myport" {
  value = var.sg["sg-1"]
  
}
