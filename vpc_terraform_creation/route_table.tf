/* 
*Routig tabels for subnets
*/
# public route table

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.myvpc.id
    tags = {
      Name = "public_route_table"
  }
}


# private route table

resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "private_route_table"
  }
}