/*
* VPC variables
*/
//vpc_id = "aws_vpc.myvpc.id"
region = "us-east-1"
vpc_cidr = "192.168.0.0/16"
//=========================================//
/*
* Subnet variables
*/
//pb_subnet_id = "aws_subnet.public_subnet.id"
public_subnet_cidr = "192.168.1.0/24"
public_subnet_avilability_zone = "us-east-1a"

//pv_subnet_id = "aws_subnet.private_subnet.id"
 private_subnet_cidr = "192.168.2.0/24"
 private_subnet_avilability_zone = "us-east-1b"
//==========================================//
/* 
* Route table variables
*/
//pb_rt_id = "aws_route_table.public_rt.id"
//pv_rt_id = "aws_route_table.private_rt.id"
//==========================================//
/*
* INTERNET GATEWAY variables
*/
dst_cidr_block = "0.0.0.0/0"
//ig_id = "aws_internet_gateway.IG.id"