//creating routes for public route tabels

resource "aws_route" "public_rt_entry" {
    route_table_id = aws_route_table.public_rt.id
    destination_cidr_block = var.dst_cidr_block
    gateway_id = aws_internet_gateway.IG.id
}