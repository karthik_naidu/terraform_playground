/* 
* INTERNET GATEWAY ceration
*/
resource "aws_internet_gateway" "IG" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "pub-IG"
  }
}