/* creation of ec-s instane
*/
resource "aws_instance" "myinstance" {
  for_each = var.instance
  ami =  each.value["ami"]
  instance_type = each.value["itype"]
  availability_zone = each.value.avz
  associate_public_ip_address = true
  vpc_security_group_ids      = [ aws_security_group.my-sec-myvpc["sg-1"].id]
  key_name = each.value["kname"]
  subnet_id = aws_subnet.public_subnet.id
  tags = {
    Name = each.value["tag"]
  }
}

/*attaching a security group to an instance 
resource "aws_network_interface_sg_attachment" "sg" {
  security_group_id = "${aws_security_group.my-sec-myvpc.id}"
  network_interface_id = aws_instance.myinstance.primary_network_interface_id
}*/

