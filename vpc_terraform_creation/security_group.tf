resource "aws_security_group" "my-sec-myvpc" {
  name        = each.value["name"]
  vpc_id = aws_vpc.myvpc.id
  description = "Allow ssh inbound traffic"
  for_each = var.sg
    ingress {
    description = "allow SSH from myvpc"
    from_port   = each.value.port
    to_port = each.value.port
    protocol = each.value["protocol"]
    cidr_blocks = each.value["cidr_block"]
 /*
  dynamic "ingress" {
    for_each = var.sg
    content {
    from_port = ingress.value["port"]
    to_port = ingress.value["port"]
    cidr_blocks = ingress.value["cidr_block"]
    protocol = ingress.value["protocol"]

    }
    */
    engress {
    description = "allow SSH from myvpc"
    from_port   = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

   tags = {
    Name = "mylove"
    }
}
 