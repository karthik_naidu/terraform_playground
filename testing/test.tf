#provider block
provider "aws" {
  region = var.region
  
}
// creation of ec-s instane
resource "aws_instance" "myinstance" {
  ami =  var.ami
  instance_type = var.i_type
  key_name = var.k_name
  vpc_security_group_ids = [aws_security_group.mysecgroup["sg-1"].id]
  tags = {
    Name = "mylove"
  }
}
// creation of security group
resource "aws_security_group" "mysecgroup" {
  name = "test-secgroup"
  for_each = var.sg
  ingress {
    cidr_blocks = each.value["cidr_block"]
    description = "Allows SSH"
    from_port = each.value["fport"]
    protocol = each.value["protocol"]
    to_port = each.value["tport"]
  } 
}