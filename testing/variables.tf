variable "region" {
  
}
variable "ami" {
  
}
variable "i_type" {
  
}
variable "k_name" {

} 
/*variable "fport" {
  
}
variable "tport" {
  
}
variable "pcol" {
  
}*/

variable "sg" {
    type = map(object({
    fport = number
    tport = number
    protocol = string
    cidr_block = list(string)

    }))
    default = {
      "sg-1" = {
        cidr_block = ["0.0.0.0/0"]
        fport = 22
        protocol = "tcp"
        tport = 22
      }
    }
  
}
