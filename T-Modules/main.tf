#provider block
provider "aws" {
  region = "us-east-1"
}

variable "names" {
 type = map(object({
   name = string
 }))

 default = {
   "one" = {
     name = "mylove"
   }
   "two" = {
     name = "I_am"
   }
 }
}

resource "aws_iam_user" "the_accounts"{

    for_each = var.names
    name = each.value["name"]
}
