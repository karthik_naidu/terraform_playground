/*
* Creation of S3 Bucket
*/
resource "aws_s3_bucket" "mylove-01" {
    bucket = "love-terraform-state"
    tags = {
        Name = "S3 Remote Terraform State Store"
    } 
}
/*
* Creation of Dynamo DB tabel 

resource "aws_dynamodb_table" "terraform-lock" {
    name = "dynamodb-01"
    read_capacity = "2"
    write_capacity = "2"
    hash_key = "LockID"
    attribute {
      name = "LockID"
      type = "S"
    }
  tags = {
    Name = "DynamoDB Terraform State Lock Table"
  }
}*/